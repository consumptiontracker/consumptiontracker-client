import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NotifierModule } from "angular-notifier";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CollapseComponent } from "./components/collapse/collapse.component";
import { HeaderComponent } from "./components/header/header.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { MeasurementComponent } from "./components/measurement/measurement.component";
import { MeasurementsComponent } from "./components/measurements/measurements.component";
import { MenuComponent } from "./components/menu/menu.component";
import { PeriodComponent } from "./components/period/period.component";
import { StatsComponent } from "./components/stats/stats.component";
import { ErrorInterceptor } from "./interceptors/error.interceptor";
import { JwtInterceptor } from "./interceptors/jwt.interceptor";
import { DatePipe } from "./pipes/date-pipe";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    NotifierModule.withConfig({
      position: {
        horizontal: {
          position: "right"
        },
        vertical: {
          position: "top"
        }
      },
      behaviour: {
        autoHide: 2000,
        onClick: "hide",
        showDismissButton: false,
        stacking: 4
      }
    })
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    PeriodComponent,
    MeasurementsComponent,
    DatePipe,
    MenuComponent,
    StatsComponent,
    CollapseComponent,
    LoginComponent,
    HomeComponent,
    MeasurementComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
