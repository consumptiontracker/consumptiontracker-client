export class GaugeDto {
    id: number;
    name: string;
    order: number;
    type: string;
    active: boolean;
    unit: string;
}
