export class MeasurementDto {
    id: number;
    gaugeId: number;
    value: number;
    recordingDate: string;
    offset: number;
}
