import { MeasurementEntity } from "../model/measurement-entity";
import { GaugeEntity } from "../model/gauge-entity";
import * as moment from "moment";

export class HelperFunctions {
  public static parseUnit(initialUnitValue: string): string {
    switch (initialUnitValue) {
      case "m3":
        return "m<sup>3</sup>";
      default:
        return initialUnitValue;
    }
  }

  public static getPreviousMeasurement(current: MeasurementEntity, all: MeasurementEntity[]): MeasurementEntity {
    return this.getPreviousMeasurementForRecordingDate(current.recordingDate, current.gauge, all);
  }

  public static getPreviousMeasurementForRecordingDate(recordingDate: moment.Moment, gauge: GaugeEntity, all: MeasurementEntity[]) {
    return all.find(
      measurementEntity =>
        measurementEntity.recordingDate.isBefore(recordingDate) &&
        measurementEntity.gauge.type == gauge.type &&
        measurementEntity.gauge.name == gauge.name
    );
  }
}
