import * as moment from 'moment';
import { MeasurementDto } from '../dto/measurement-dto';
import { GaugeEntity } from './gauge-entity';

export class MeasurementEntity {
    id: number;
    value: number;
    consumption: number;
    recordingDate: moment.Moment;
    offset: number;
    gauge: GaugeEntity;

    constructor (measurementDto: MeasurementDto, gaugeEntity: GaugeEntity) {
        if (measurementDto) {
            this.id = measurementDto.id;
            this.value = measurementDto.value;
            this.recordingDate = moment(measurementDto.recordingDate);
            this.offset = measurementDto.offset;
        }
        this.gauge = gaugeEntity;
    }
}