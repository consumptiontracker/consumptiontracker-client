import * as moment from "moment";
import { MeasurementEntity } from "./measurement-entity";

export class StatsEntity {
  periods: PeriodStatsEntity[] = [];

  constructor(measurementEntities: MeasurementEntity[]) {
    if (measurementEntities.length > 0) {
      let periodStatsMap: Map<string, MeasurementEntity[]> = new Map();
      measurementEntities.forEach(measurementEntity => {
        if (!periodStatsMap.get(measurementEntity.recordingDate.toString())) {
          periodStatsMap.set(measurementEntity.recordingDate.toString(), []);
        }
        periodStatsMap.get(measurementEntity.recordingDate.toString()).push(measurementEntity);
      });

      Array.from(periodStatsMap.values()).forEach(measurementEntities => this.periods.push(new PeriodStatsEntity(measurementEntities)));
    }
  }
}

export class PeriodStatsEntity {
  recordingDate: moment.Moment;
  types: GaugeTypeStatsEntity[] = [];

  constructor(measurementEntities: MeasurementEntity[]) {
    this.recordingDate = measurementEntities[0].recordingDate;

    let gaugeTypeStatsMap: Map<string, MeasurementEntity[]> = new Map();
    measurementEntities.forEach(measurementEntity => {
      if (!gaugeTypeStatsMap.get(measurementEntity.gauge.type)) {
        gaugeTypeStatsMap.set(measurementEntity.gauge.type, []);
      }
      gaugeTypeStatsMap.get(measurementEntity.gauge.type).push(measurementEntity);
    });

    Array.from(gaugeTypeStatsMap.values()).forEach(measurementEntities => this.types.push(new GaugeTypeStatsEntity(measurementEntities)));
  }
}

export class GaugeTypeStatsEntity {
  type: string;
  measurements: MeasurementStatsEntity[] = [];
  consumption: number;
  unit: string;

  constructor(measurementEntities: MeasurementEntity[]) {
    this.type = measurementEntities[0].gauge.type;
    this.unit = measurementEntities[0].gauge.unit;
    measurementEntities.forEach(measurementEntity => this.measurements.push(new MeasurementStatsEntity(measurementEntity)));
    this.measurements.sort((m1, m2) => m1.gaugeOrder - m2.gaugeOrder);
    this.consumption = this.measurements.map(measurement => measurement.consumption).reduce((p, c) => p + c);
  }
}

export class MeasurementStatsEntity {
  gaugeName: string;
  gaugeOrder: number;
  value: number;
  consumption: number;

  constructor(measurementEntity: MeasurementEntity) {
    this.gaugeName = measurementEntity.gauge.name;
    this.gaugeOrder = measurementEntity.gauge.order;
    this.value = measurementEntity.value;
    this.consumption = measurementEntity.consumption;
  }
}
