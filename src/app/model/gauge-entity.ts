import { GaugeDto } from '../dto/gauge-dto';
import { HelperFunctions } from '../util/helper-functions';

export class GaugeEntity {
    id: number;
    name: string;
    order: number;
    type: string;
    active: boolean;
    unit: string;
    
    constructor (gaugeDto: GaugeDto) {
        Object.assign(this, gaugeDto);
        this.unit = HelperFunctions.parseUnit(this.unit);
    }
}