import { GaugeEntity } from "./gauge-entity";
import { GaugeDto } from "../dto/gauge-dto";
import { MeasurementDto } from "../dto/measurement-dto";
import { MeasurementEntity } from "./measurement-entity";
import { HelperFunctions } from "../util/helper-functions";

export class EntityTransformer {
  public static gaugeDtosToEntities(gaugeDtos: GaugeDto[]): GaugeEntity[] {
    return gaugeDtos.map(gaugeDto => new GaugeEntity(gaugeDto));
  }

  public static measurementDtosToEntities(measurementDtos: MeasurementDto[], gaugeEntities: GaugeEntity[]): MeasurementEntity[] {
    let measurementEntities: MeasurementEntity[] = measurementDtos.map(
      measurementDto => new MeasurementEntity(measurementDto, gaugeEntities.find(gaugeEntity => gaugeEntity.id == measurementDto.gaugeId))
    );
    measurementEntities.forEach(measurement => (measurement.consumption = this.getConsumption(measurement, measurementEntities)));
    return measurementEntities;
  }

  public static measurementEntitiesToDtos(measurementEntities: MeasurementEntity[]): MeasurementDto[] {
    return measurementEntities.map(measurementEntity => this.measurementEntityToDto(measurementEntity));
  }

  public static measurementEntityToDto(measurementEntity: MeasurementEntity): MeasurementDto {
    const measurementDto = new MeasurementDto();
    measurementDto.id = measurementEntity.id;
    measurementDto.gaugeId = measurementEntity.gauge.id;
    measurementDto.value = measurementEntity.value;
    measurementDto.recordingDate = measurementEntity.recordingDate.format("YYYY-MM-DD");
    measurementDto.offset = measurementEntity.offset;
    return measurementDto;
  }

  private static getConsumption(current: MeasurementEntity, all: MeasurementEntity[]): number {
    const previousMeasurement = HelperFunctions.getPreviousMeasurement(current, all);
    const currentDelta = current.value - current.offset;
    const previousDelta = previousMeasurement ? previousMeasurement.value - previousMeasurement.offset : 0;
    return currentDelta - previousDelta;
  }
}
