import { Component, OnInit } from "@angular/core";
import { StateService } from "../../services/state.service";
import { ConsumptionService } from "src/app/services/consumption.service";
import { MeasurementEntity } from "src/app/model/measurement-entity";

@Component({
  selector: "app-measurements",
  templateUrl: "./measurements.component.html",
  styleUrls: ["./measurements.component.css"]
})
export class MeasurementsComponent implements OnInit {
  saveAllEnabled: boolean = false;

  constructor(public stateService: StateService, private consumptionService: ConsumptionService) {}

  ngOnInit() {}

  saveAll() {
    const measurementsToSave: MeasurementEntity[] = this.stateService.selectedMeasurements.filter(measurement => measurement.value >= 0);
    this.consumptionService.saveMeasurements(measurementsToSave);
  }
}
