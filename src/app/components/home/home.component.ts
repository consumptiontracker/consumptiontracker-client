import { Component, OnInit } from "@angular/core";
import { ConsumptionService } from "../../services/consumption.service";
import { StateService } from "../../services/state.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(
    public stateService: StateService,
    private networkingService: ConsumptionService
  ) {}

  ngOnInit() {
    this.networkingService.loadServerState();
  }
}
