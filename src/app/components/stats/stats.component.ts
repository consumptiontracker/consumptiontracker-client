import { Component, OnInit } from "@angular/core";
import { StateService } from "../../services/state.service";

@Component({
  selector: "app-stats",
  templateUrl: "./stats.component.html",
  styleUrls: ["./stats.component.css"]
})
export class StatsComponent implements OnInit {
  showConsumption: boolean = false;

  constructor(public stateService: StateService) {}

  ngOnInit() {}
}
