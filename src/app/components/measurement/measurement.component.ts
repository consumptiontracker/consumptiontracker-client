import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { StateService } from "../../services/state.service";
import { ConsumptionService } from "src/app/services/consumption.service";
import { MeasurementEntity } from "src/app/model/measurement-entity";

@Component({
  selector: "app-measurement",
  templateUrl: "./measurement.component.html",
  styleUrls: ["./measurement.component.css"]
})
export class MeasurementComponent implements OnInit {
  @Input() measurement: MeasurementEntity;
  @Output() measurementChange = new EventEmitter();
  saveEnabled: boolean = false;
  measurementTitle: string = "Measurement";
  offsetTitle: string = "Offset";
  deleteConfirmVisible: boolean = false;

  constructor(private consumptionService: ConsumptionService) {}

  ngOnInit() {}

  setValueChanged() {
    this.measurementTitle = this.measurement.value > 0 ? "Measurement*" : "Measurement";
    this.saveEnabled = this.measurement.value > 0;
    this.measurementChange.emit();
  }

  setOffsetChanged() {
    this.offsetTitle = "Offset*";
    this.saveEnabled = this.measurement.value > 0;
    this.measurementChange.emit();
  }

  delete() {
    this.deleteConfirmVisible = true;
  }

  deleteConfirm() {
    this.consumptionService.deleteMeasurement(this.measurement.id);
  }

  deleteCancel() {
    this.deleteConfirmVisible = false;
  }

  save() {
    this.consumptionService.saveMeasurements([this.measurement]);
  }
}
