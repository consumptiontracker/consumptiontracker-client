import { Component, Input, OnInit } from '@angular/core';
import { PeriodStatsEntity } from '../../model/stats-entity';

@Component({
  selector: 'app-collapse',
  templateUrl: './collapse.component.html',
  styleUrls: ['./collapse.component.css']
})
export class CollapseComponent implements OnInit {
  @Input() periodStats: PeriodStatsEntity;
  @Input() showConsumption: boolean;
  
  isCollapsed: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
