import { Component, OnInit } from '@angular/core';
import { StateService } from '../../services/state.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public stateService: StateService) { }

  ngOnInit() {
  }

  toggleMenu() {
    this.stateService.menuVisible = !this.stateService.menuVisible;
  }

}
