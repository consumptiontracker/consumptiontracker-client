import { Injectable } from "@angular/core";
import * as moment from "moment";
import { GaugeEntity } from "../model/gauge-entity";
import { MeasurementEntity } from "../model/measurement-entity";
import { StatsEntity } from "../model/stats-entity";
import { GaugeDto } from "../dto/gauge-dto";
import { EntityTransformer } from "../model/entity-transformer";
import { MeasurementDto } from "../dto/measurement-dto";
import { HelperFunctions } from "../util/helper-functions";

@Injectable({
  providedIn: "root"
})
export class StateService {
  menuVisible: boolean = false;

  periodOffset: number = 0;
  selectedDate: moment.Moment = moment().endOf("month");

  gauges: GaugeEntity[] = [];
  measurements: MeasurementEntity[] = [];
  stats: StatsEntity;
  selectedMeasurements: MeasurementEntity[] = [];

  constructor() {}

  loadGauges(gaugeDtos: GaugeDto[]) {
    gaugeDtos.sort((g1, g2) => g1.order - g2.order);
    this.gauges = EntityTransformer.gaugeDtosToEntities(gaugeDtos);
  }

  loadMeasurements(measurementDtos: MeasurementDto[]) {
    measurementDtos.sort((m1, m2) => m2.recordingDate.localeCompare(m1.recordingDate));
    this.measurements = EntityTransformer.measurementDtosToEntities(measurementDtos, this.gauges);
  }

  refreshSelectedMeasurements() {
    let selectedMeasurements: MeasurementEntity[] = [];
    this.gauges.forEach(gauge => {
      let selectedGaugeMeasurement = this.measurements.find(
        measurement =>
          measurement.recordingDate.format("YYYY-MM-DD") == this.selectedDate.format("YYYY-MM-DD") &&
          measurement.gauge.type == gauge.type &&
          measurement.gauge.name == gauge.name
      );
      if (!selectedGaugeMeasurement) {
        const previousMeasurement = HelperFunctions.getPreviousMeasurementForRecordingDate(this.selectedDate, gauge, this.measurements);
        selectedGaugeMeasurement = new MeasurementEntity(null, gauge);
        selectedGaugeMeasurement.offset = previousMeasurement ? previousMeasurement.offset : 0;
        selectedGaugeMeasurement.recordingDate = this.selectedDate;
      }

      selectedMeasurements.push(selectedGaugeMeasurement);
    });

    this.selectedMeasurements = selectedMeasurements;
  }

  refreshStats() {
    this.stats = new StatsEntity(this.measurements);
  }

  refreshSelectedDate() {
    this.selectedDate = moment()
      .add(this.periodOffset, "month")
      .endOf("month");
    this.refreshSelectedMeasurements();
  }

  increaseSelectedMonth() {
    this.periodOffset += 1;
    this.refreshSelectedDate();
  }

  decreaseSelectedMonth() {
    this.periodOffset -= 1;
    this.refreshSelectedDate();
  }
}
