import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { NotifierService } from "angular-notifier";
import { catchError, retry } from "rxjs/operators";
import { GaugeDto } from "../dto/gauge-dto";
import { MeasurementDto } from "../dto/measurement-dto";
import { EntityTransformer } from "../model/entity-transformer";
import { MeasurementEntity } from "../model/measurement-entity";
import { ClientService } from "./client.service";
import { StateService } from "./state.service";

@Injectable({
  providedIn: "root"
})
export class ConsumptionService extends ClientService {
  private gaugesUrl: string = "/gauges";
  private measurementsUrl: string = "/measurements";

  constructor(private stateService: StateService, private http: HttpClient, notifierService: NotifierService) {
    super(notifierService);
  }

  loadServerState(): void {
    this.stateService.measurements = [];
    if (this.stateService.gauges.length < 1) {
      this.loadGauges();
    } else {
      this.loadMeasurements();
    }
  }

  saveMeasurements(measurements: MeasurementEntity[]): void {
    const measurementDtos = EntityTransformer.measurementEntitiesToDtos(measurements);
    this.http
      .post<MeasurementDto[]>(this.env.apiUrl + this.measurementsUrl, measurementDtos)
      .pipe(catchError(error => this.handleError(error)))
      .subscribe(() => {
        this.notifierService.notify("success", "Successfully saved!");
        this.loadServerState();
      });
  }

  deleteMeasurement(id: number): void {
    this.http
      .delete(this.env.apiUrl + this.measurementsUrl + "/" + id)
      .pipe(catchError(error => this.handleError(error)))
      .subscribe(() => {
        this.notifierService.notify("success", "Successfully deleted!");
        this.loadServerState();
      });
  }

  private loadGauges(): void {
    this.http
      .get<GaugeDto[]>(this.env.apiUrl + this.gaugesUrl)
      .pipe(
        retry(3),
        catchError(error => this.handleError(error))
      )
      .subscribe(gaugeDtos => {
        this.stateService.loadGauges(gaugeDtos);
        this.loadMeasurements();
      });
  }

  private loadMeasurements() {
    this.http
      .get<MeasurementDto[]>(this.env.apiUrl + this.measurementsUrl)
      .pipe(
        retry(3),
        catchError(error => this.handleError(error))
      )
      .subscribe(measurementDtos => {
        this.stateService.loadMeasurements(measurementDtos);
        this.stateService.refreshSelectedMeasurements();
        this.stateService.refreshStats();
      });
  }
}
